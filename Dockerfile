
FROM maven:3.8.4-openjdk-11-slim AS build

# Set the working directory in the container
WORKDIR /app

# Copy the Maven project file
COPY pom.xml .

# Copy the rest of the application code
COPY src ./src

# Build the application with Mavencd
RUN mvn package -DskipTests

# Set up the runtime image
FROM openjdk:11-jre-slim

# Copy the built JAR file from the build image
COPY --from=build /app/target/*.jar /app/app.jar

# Set the startup command to run the JAR file
CMD ["java", "-jar", "/app/app.jar"]
